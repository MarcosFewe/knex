const knex = require('../database');

module.exports = {
    async user(req, res) { 
        const results = await knex('usuarios');

        return res.json(results)
    }
}